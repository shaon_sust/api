<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Task;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;


class TaskController extends FOSRestController
{

    /**
     * @Rest\Get("/task")
     */
    public function getTasksAction()
    {
        $tasks = $this->getDoctrine()->getRepository("AppBundle:Task")
            ->findAll();

        return array('tasks' => $tasks);
    }

    /**
     * @Rest\Get("/task/{id}",)
     */
    public function getTaskAction($id)
    {
        $task = $this->getDoctrine()->getRepository('AppBundle:Task')->find($id);
        if($task === null)
        {
            return new View('Task not found', Response::HTTP_NOT_FOUND);
        }

        return $task;
    }

    /**
     * @Rest\Post("/task")
     */
    public function postTaskAction(Request $request)
    {
        $task = new Task();
        $name = $request->get('name');
        $description = $request->get('description');
        $is_done = $request->get('is_done');
        if(empty($name) || empty($description) || empty($is_done))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }

        $task->setName($name);
        $task->setDescription($description);
        $task->setIsDone($is_done);

        $em = $this->getDoctrine()->getManager();
        $em->persist($task);
        $em->flush();

        return new View("Task Added Successfully", Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/task/{id}")
     */
    public function putTaskAction($id, Request $request)
    {
        $task = $this->getDoctrine()->getRepository('AppBundle:Task')->find($id);
        if(empty($task))
        {
            return new View('User not found', Response::HTTP_NOT_FOUND);
        }

        $name = $request->get('name');
        $description = $request->get('description');
        $is_done = $request->get('is_done');

        if(empty($name) && empty($description) && empty($is_done))
        {
            return new View("NULL VALUES ARE NOT ALLOWED", Response::HTTP_NOT_ACCEPTABLE);
        }
        elseif (!empty($name) && empty($description) && empty($is_done))
        {
            $em = $this->getDoctrine()->getManager();
            $task->setName($name);
            $em->flush();
            return new View("name Updated Successfully", Response::HTTP_OK);
        }
        elseif (empty($name) && !empty($description) && empty($is_done))
        {
            $em = $this->getDoctrine()->getManager();
            $task->setDescription($description);
            $em->flush();
            return new View("description Updated Successfully", Response::HTTP_OK);
        }
        elseif (empty($name) && empty($description) && !empty($is_done))
        {
            $em = $this->getDoctrine()->getManager();
            $task->setIsDone($is_done);
            $em->flush();
            return new View("isDone Updated Successfully", Response::HTTP_OK);
        }
        elseif (!empty($name) && !empty($description) && empty($is_done))
        {
            $em = $this->getDoctrine()->getManager();
            $task->setName($name);
            $task->setDescription($description);
            $em->flush();
            return new View("name and description Updated Successfully", Response::HTTP_OK);
        }
        elseif (!empty($name) && empty($description) && !empty($is_done))
        {
            $em = $this->getDoctrine()->getManager();
            $task->setName($name);
            $task->setIsDone($is_done);
            $em->flush();
            return new View("name and isDone Updated Successfully", Response::HTTP_OK);
        }
        elseif (empty($name) && !empty($description) && !empty($is_done))
        {
            $em = $this->getDoctrine()->getManager();
            $task->setIsDone($is_done);
            $task->setDescription($description);
            $em->flush();
            return new View("isDone and description Updated Successfully", Response::HTTP_OK);
        }
        elseif (!empty($name) && !empty($description) && !empty($is_done))
        {
            $em = $this->getDoctrine()->getManager();
            $task->setName($name);
            $task->setDescription($description);
            $task->setIsDone($is_done);
            $em->flush();
            return new View("name, description and IsDone Updated Successfully", Response::HTTP_OK);
        }
    }

    /**
     * @Rest\Delete("/task/{id}")
     */
    public function deleteTaskAction($id)
    {
        $task = $this->getDoctrine()->getRepository('AppBundle:Task')->find($id);
        if (empty($task))
        {
            return new View('Task not found', Response::HTTP_NOT_FOUND);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($task);
        $em->flush();

        return new View('Task deleted successfully', Response::HTTP_OK);
    }
}
